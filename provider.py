#!/usr/bin/env python3

import requests

class Provider():
   name = ""
   headers = {}
   method = ""
   urlCpe = ""
   urlSearch = ""
   params = None
   console = None

   def __init__(self,name,headers,method,urlCpe,urlSearch,params,consolelog):
      self.name = name
      self.headers = headers
      self.method = method
      self.urlCpe = urlCpe
      self.urlSearch = urlSearch
      self.params = params
      self.console = consolelog

   def query(self, query, isCpe ):
      
      urlUsage = self.urlSearch
      if isCpe:
         urlUsage = self.urlCpe
      if self.method == "POST":
          response = requests.post(urlUsage,headers=self.headers,data=self.params)
      elif self.method == "GET":
          urlUsage+=query
          response = requests.get(urlUsage,headers=self.headers)
      
      return response

