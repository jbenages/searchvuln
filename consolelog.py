#!/usr/bin/env python3

from termcolor import colored, cprint

class ConsoleLog():
   verbose = 0
   colors = { "info": "blue","error": "red","done": "green","undone": "yellow" }
   symbol = { "info": "i","error": "!","done": "+","undone": "-" }
   verboseLevel = { "info": 3, "error": 2, "done": 1, "undone": 1 }

   def __init__(self, verbose):
      self.verbose = verbose

   def output(self, output, level ):

      if self.verbose >= self.verboseLevel[level]:
         cprint("["+self.symbol[level]+"]", self.colors[level], attrs=['bold'], end='' )
         cprint(output)
