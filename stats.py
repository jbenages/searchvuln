#!/usr/bin/env python3

from pprint import pprint

class Stats():
  
   consoleLog = None

   providers = {}

   def __init__(self, consoleLog):
      self.consoleLog = consoleLog

   def addProvider(self, provider, response):
      self.providers[provider] = response

   def countCVES(self):
      pprint(self.providers)
      for provider in self.providers:
         self.consoleLog.output( " "+provider+" Tota vulns:"+str(len(self.providers[provider])), "done" )
