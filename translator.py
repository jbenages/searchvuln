#!/usr/bin/env python3

from pprint import pprint

class Translator():

   def translateToCVEList(self, providers):
      cveListFinal = {}
      for provider in providers:
#         pprint(providers[provider]["cveList"])
         for cveRow in providers[provider]["cveList"]:
            cveID = cveRow 
            for id in providers[provider]["cveId"]:
               cveID = cveID[id]
            if cveID not in cveListFinal:
               cveListFinal[cveID] = { "providers": {} }
            cveListFinal[cveID]["providers"][provider] = {}
            for info in providers[provider]['infoCVE']:
               infoText = cveRow
               for path in providers[provider]['infoCVE'][info]:
                  infoText = infoText[path]
               cveListFinal[cveID]["providers"][provider][info] = infoText
      pprint(cveListFinal)

   def translateResponse(self, providerPath, response ):
      newResponse = response
      for path in providerPath:
         if path in newResponse:
            newResponse = newResponse[path]
         else:
            newResponse = False
            break

      return newResponse
