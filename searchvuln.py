#!/usr/bin/env python3

import requests,sys,os,yaml,argparse,json,re
from termcolor import colored, cprint
from pprint import pprint
from provider import Provider
from consolelog import ConsoleLog
from stats import Stats
from translator import Translator

config = yaml.safe_load(open(os.path.abspath(sys.path[0])+"/config.yml"))

parser = argparse.ArgumentParser()
parser.add_argument('-s', dest='search', help='String to search: Microsoft Explorer or by cpe format: cpe:2.3:o:microsoft:*:*:*:*:*:*:*:*:*')
args = parser.parse_args()

consoleLog = ConsoleLog(3)
stats = Stats(consoleLog)
translator = Translator()

providers = { 
   "circllu" : {
      "headers" : {'User-Agent': config["user-agent"]},
      "method": "GET",
      "urls": {
         "cpe" : "http://cve.circl.lu/api/cvefor/",
         "browse" : "http://cve.circl.lu/api/browse/",
         "search" : "http://cve.circl.lu/api/search/"
         },
      "params": None,
      "cvePath" : [ ],
      "cveId" : [ "id" ],
      "infoCVE": {
         "references" : [ "references" ],
         "summary": [ "summary" ]
         }
      },
   "nist" : {
      "headers" : {'User-Agent': config["user-agent"]},
      "method": "GET",
      "urls": {
         "cpe" : "https://services.nvd.nist.gov/rest/json/cves/1.0?startIndex=0&resultsPerPage=0&includeDeprecated=true&cpeMatchString=",
         "search" : "https://services.nvd.nist.gov/rest/json/cves/1.0?keyword="
         },
      "params": { },
      "cvePath" : [ "result", "CVE_Items" ],
      "cveId" : [ "cve","CVE_data_meta","ID"],
      "infoCVE": {
         "references" : [ "cve", "references", "reference_data" ] ,
         "summary": [ "cve", "description", "description_data", 0, "value" ],
         "severity": [ "impact", "baseMetricV2", "severity" ]
         }
      },
#    "vulndb" : {
#       "headers" : {'User-Agent': config["user-agent"], 'X-VulDB-ApiKey': config["vulndb-apikey"]},
#       "method": "POST",
#       "urls": {
#          "cpe": "https://vuldb.com/?api",
#          "search" : "https://vuldb.com/?api",
#          },
#       "params": { "search" : None, "details" : 0}
#       }
   }

def checkIsCpe( query ):
   cpeReg = re.compile('cpe:2\.3:[aho](?::(?:[a-zA-Z0-9!"#$%&\'()*+,\\-_.\/;<=>?@\[\]^`{|}~]|\\:)+){10}$')
   isCpe = False
   if cpeReg.match(query):
      consoleLog.output(" CPE detected.", "info")
      isCpe = True
   else:
      consoleLog.output(" Words detected.", "info")
   return isCpe

def checkErrors( response ):
   wrongResponse = False

   if response.status_code != 200:
      consoleLog.output( " Error in connection in provider, code:"+str(response.status_code), "error" )
      wrongResponse = True
   
   return wrongResponse

def searchInProviders( search ):
   isCpe = checkIsCpe(search)

   for oneProvider in providers:
      pprint( oneProvider )
      provider = Provider(oneProvider,providers[oneProvider]["headers"],providers[oneProvider]["method"],providers[oneProvider]["urls"]["cpe"],providers[oneProvider]["urls"]["search"],providers[oneProvider]["params"],consoleLog)
      response = provider.query(search, isCpe)
      if checkErrors(response) == False:
         responseJson = json.loads(response.content)
         responseTranslated = translator.translateResponse(providers[oneProvider]["cvePath"],responseJson)
         if responseTranslated != False:
            providers[oneProvider]["cveList"] = translator.translateResponse(providers[oneProvider]["cvePath"],responseJson)
         else:
            providers[oneProvider]["cveList"] = {}
            consoleLog.output( " Error extract CVE data in :"+oneProvider, "error" )
            #pprint(responseJson)
   
   translator.translateToCVEList(providers)
#   stats.addProvider(oneProvider, responseJson)
#   stats.countCVES()

def vulnDB( search ):
   headers = {'User-Agent': config["user-agent"], 'X-VulDB-ApiKey': config["vulndb-apikey"]}
   postData = {}
   # URL VulDB endpoint
   url = 'https://vuldb.com/?api'
   
   postData['search'] = search 
   postData['details'] = 0 
   
   # Get API response
   response = requests.post(url,headers=headers,data=postData)
   
   # Display result if evertything went OK
   if response.status_code == 200:
   
       # Parse HTTP body as JSON
       responseJson = json.loads(response.content)
       pprint( json.dumps(responseJson, indent=4, sort_keys=True) )       
       # Output
#        for i in responseJson['result']:        
#            print(i['entry'])
            #print(i["entry"]["id"])
            #print(i["entry"]["title"])

#vulnDB( args.search )
searchInProviders( args.search )

